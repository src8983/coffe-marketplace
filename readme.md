Run in a Docker:

docker-compose build
docker-compose up

Run the test:

docker exec -it {$name_docker} /bin/bash npm run test
