'use strict'

const mongoose = require('mongoose');
const app = require('./app');
const PORT = 3000;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mongo:27017/coffe_marketplace')

if(!module.parent) {
   app.listen();
}

module.exports = app.listen(PORT)
