'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// Roting
app.get('/', (req, res) => {
  res.send('Mi Hello world\n');
});

app.use('/api/user', require('./routers/user') );
app.use('/api/coffe', require('./routers/coffe') );

module.exports = app;
