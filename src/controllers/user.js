'use strict'

var User = require('../models/user');
const config = require('../config');

function test (req,res) {
	res.status(200).send({
		message : 'Action test user'
	});
}

function view (req,res) {

	User.findById(req.params.id, (err,user) => {

		if(err)
			return res.status(500).send({message:'Error request'});

		if(!user)
			return res.status(404).send({message:'User doesn´t exists'});

		return res.status(200).send({ user: user.view() });

	});
}

function createAdmin (req, res) {

	var params = req.body;

	var user = new User();
	user.name = params.name;
	user.surname = params.surname;
	user.email = params.email;
	user.password = params.password;
	user.role = config.admin;
	user.save( (err, userStored) => {

		if(err)
			return res.status(500).send({message : 'Problem saving the user'});

		if(userStored)
			res.status(200).send({user : userStored});
		else
			res.status(404).send({message : 'User not registered'});
	});
}

function createCustomer(req, res) {

	var params = req.body;

	var user = new User();
	user.name = params.name;
	user.surname = params.surname;
	user.email = params.email;
	user.password = params.password;
	user.role = config.customer;
	user.save( (err, userStored) => {

		if(err)
			return res.status(500).send({message : 'Problem saving the user'});

		if(userStored)
			res.status(200).send({user : userStored});
		else
			res.status(404).send({message : 'User not registered'});
	});
}

function login(req,res){
	var params = req.body;

	User.findOne({email: params.email},(err,user)=>{

		if(err)
			return res.status(500).send({message: 'Error request'});

		if(user)
		{
			user.comparePassword(params.password,(err,check)=> {
				if(check)
				{
					//if(params.gettoken)
					return res.status(200).send({token: user.createToken()});
					//else
					//	return res.status(200).send({  user: user.view() });
				}
				else
					return res.status(404).send({ message: 'Invalid password'});
			});
		}
		else
			res.status(404).send({message : 'Usuario no registrado'});
	});
}

module.exports = {
  test,
	view,
	createAdmin,
	createCustomer,
	login
}
