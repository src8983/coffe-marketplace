'use strict'

var Order = require('../models/order');

function create (req, res) {

	var order = new Order(req.body);

	coffe.save( (err, orderStored) => {

		if(err)
			return res.status(500).send({message : 'Problem saving the order'});

		if(orderStored)
			res.status(200).send({order : orderStored});
		else
			res.status(404).send({message : 'Order not registered'});
	});
}

module.exports = {
  create
}
