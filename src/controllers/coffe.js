'use strict'

var Coffe = require('../models/coffe');

function create (req, res) {

	var coffe = new Coffe(req.body);

	coffe.save( (err, coffeStored) => {

		if(err)
			return res.status(500).send({message : 'Problem saving the coffe'});

		if(coffeStored)
			res.status(200).send({coffe : coffeStored});
		else
			res.status(404).send({message : 'Coffe not registered'});
	});
}

function update (req, res) {

	Coffe.findByIdAndUpdate(req.params.id,req.body,(err, coffeStored) => {

		if(err)
			return res.status(500).send({message : 'Problem saving the coffe'});

		if(coffeStored)
			res.status(200).send({coffe : coffeStored});
		else
			res.status(404).send({message : 'Coffe not saved'});
	});
}

module.exports = {
  create,
	update
}
