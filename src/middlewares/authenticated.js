'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');
const secret = config.jwt_secret;

exports.ensureAuth = function(req, res, next){

	if(!req.headers.authorization)
		return res.status(403).send({
			message: 'Non authorization data in header'
		});

	var token = req.headers.authorization.replace(/['"]+/g,'');

	try
	{
		var payload = jwt.decode(token, secret);

		if(payload.exp <= moment().unix())
		{
			return res.status(401).send({
				message: 'expired token'
			});
		}
	}
	catch(ex)
	{
		return res.status(404).send({
				message: 'no valid token'
			});
	}

  //Put the user data in the request
	req.user = payload;

	next();
}
