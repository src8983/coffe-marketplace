'use strict'


const express = require('express');

const auth = require('../middlewares/authenticated');

const api = express.Router();

const Controller = require('../controllers/user');

api.get('/test', Controller.test);
api.post('/register/admin', Controller.createAdmin);
api.post('/register/customer', Controller.createCustomer);
api.post('/login', Controller.login);
api.get('/view/:id',auth.ensureAuth, Controller.view);

module.exports = api;
