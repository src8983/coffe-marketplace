'use strict'

const express = require('express');
const auth = require('../middlewares/authenticated');
const api = express.Router();

const Controller = require('../controllers/order');

api.post('/create',auth.ensureAuth, Controller.create);

module.exports = api;
