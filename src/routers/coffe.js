'use strict'

const express = require('express');
const auth = require('../middlewares/authenticated');
const api = express.Router();

const Controller = require('../controllers/coffe');

api.post('/create',auth.ensureAuth, Controller.create);
api.post('/update/:id',auth.ensureAuth, Controller.update);

module.exports = api;
