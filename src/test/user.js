const PORT_TESTING = 3001

let mongoose = require('mongoose');
require('sinon-mongoose');

let User = require('../models/user');
let server = require('../app.js').listen(PORT_TESTING);
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('users', () => {

  before(function (done) {
    mongoose.connect('mongodb://mongo:27017/test');
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function() {
      console.log('We are connected to test database!');
      done();
    });
  });

  after(function(done){
    //server.close();
    mongoose.connection.db.dropDatabase(function(){
      mongoose.connection.close(done);
    });

  });

  it('/POST /api/user/register/admin', (done) => {
    let userData = {
        name: "John",
        surname: "Crichton",
        email: 'src.898355@gmail.com',
        password: 'secret'
    }

    chai.request(server)
      .post('/api/user/register/admin')
      .send(userData)
      .end((err, res) => {

        res.should.have.status(200);
        res.body.should.be.an('object');
        res.body.user.should.have.property('name');
        res.body.user.should.have.property('surname');
        res.body.user.should.have.property('email');
        res.body.user.should.have.property('role').eql('admin')

        done();
      });
  });

  it('/POST /api/user/register/customer', (done) => {
    let userData = {
        name: "Jack",
        surname: "Sparrow",
        email: 'src.8983@gmail.com',
        password: 'secret'
    }

    chai.request(server)
      .post('/api/user/register/customer')
      .send(userData)
      .end((err, res) => {

        res.should.have.status(200);
        res.body.should.be.an('object');
        res.body.user.should.have.property('name');
        res.body.user.should.have.property('surname');
        res.body.user.should.have.property('email');
        res.body.user.should.have.property('role').eql('customer')

        done();
      });
  });

  it('/POST /api/user/login', (done) => {
    let userData = {
        email: 'src.898355@gmail.com',
        password: 'secret'
    }

    chai.request(server)
      .post('/api/user/login')
      .send(userData)
      .end((err, res) => {

        res.should.have.status(200);
        res.body.should.be.an('object');
        res.body.should.have.property('token');

        done();
      });
  });
});
//});
