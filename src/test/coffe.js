const PORT_TESTING = 3001

let mongoose = require('mongoose');
require('sinon-mongoose');

let User = require('../models/user');
let Coffe = require('../models/coffe');
let server = require('../app.js').listen(PORT_TESTING);
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('coffe', () => {

  before(function (done) {
    mongoose.connect('mongodb://mongo:27017/test');
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'));
    db.once('open', function() {
      console.log('We are connected to test database!');

      //Creating an admin user
      let userData = {
          name: "John",
          surname: "Crichton",
          email: 'src.898355@gmail.com',
          password: 'secret'
      }

      chai.request(server)
          .post('/api/user/register/admin')
          .send(userData)
          .end((err, res) => {

            //And login it


           done();
        });
    });
  });

  after(function(done){
    //server.close();
    mongoose.connection.db.dropDatabase(function(){
      mongoose.connection.close(done);
    });

  });

  it('/POST /api/coffe/create', (done) => {

    let userData = {
        email: 'src.898355@gmail.com',
        password: 'secret'
    }

    chai.request(server)
      .post('/api/user/login')
      .send(userData)
      .end((err, res) => {

        let coffeData = {
            name: 'Ristretto',
            intensity: 'Low',
            price: 10,
            stock: 1000,
        }

        chai.request(server)
          .post('/api/coffe/create')
          .set('Authorization', res.body.token)
          .send(coffeData)
          .end((err, res) => {

            res.should.have.status(200);
            res.body.should.be.an('object');
            res.body.coffe.should.have.property('name');
            res.body.coffe.should.have.property('intensity');
            res.body.coffe.should.have.property('price');
            res.body.coffe.should.have.property('stock')

            done();
          });
      });
  });
});
//});
