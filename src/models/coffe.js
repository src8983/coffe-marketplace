'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CoffeSchema = Schema({
	name : {type: String, required: true},
	intensity: {type: String, required: true, enum: ['Low', 'Medium', 'High']},
	price: {type: Schema.Types.Decimal128, required: true},
	stock: {type: Number, required: true, min: 0}
});

module.exports = mongoose.model('Coffe',CoffeSchema);
