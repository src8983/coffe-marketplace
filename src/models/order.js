'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = Schema({
	user_id : {type: ObjectId, required: true},
	coffe_id: {type: ObjectId, required: true},
	amount: {type: Schema.Types.Decimal128, required: true},
	quantity: {type: Number, required: true, min: 0}
});

OrderSchema.index({ user_id: 1, coffe_id: 1 }); 

module.exports = mongoose.model('Order',OrderSchema);
