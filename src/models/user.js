'use strict'

var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config');
var secret = config.jwt_secret;

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-pagination');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var UserSchema = Schema({
	name : {type: String, required: true},
	surname: {type:String, required: true},
	email: {type:String, required: true, unique : true, match: /\S+@\S+\.\S+/},
	password: {type:String, required: true},
	role: {type: String, required: true}
});

UserSchema.plugin(uniqueValidator);

UserSchema.pre('save', function(next) {
  var user = this;

	if (!user.isModified('password'))
		return next();

  bcrypt.hash(user.password,  null, null, function(err, hash) {
  	if (err) return next(err);
  	user.password = hash;
  	next();
  });

});

UserSchema.methods.view = function(){
	this.password = undefined;
	return this;
};

UserSchema.methods.createToken = function(){
	var payload = {
		sub: this._id,
		name: this.name,
		surname: this.surname,
		email: this.email,
		role: this.role,
		iat: moment().unix(),
		exp: moment().add(30, 'days').unix()
	};

	//console.log(this);

	return jwt.encode(payload, secret);
};

UserSchema.methods.comparePassword = function(candidatePassword, next) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        next(null, isMatch);
    });
};

module.exports = mongoose.model('User',UserSchema);
