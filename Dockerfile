FROM node:10

WORKDIR /src

COPY ./src/package.json /src/package.json
RUN npm install

COPY ./src /src

EXPOSE  3000
#CMD ["node", "/src/index.js"]
CMD ["./node_modules/.bin/nodemon", "/src/index.js"]
